package ru.tsc.tambovtsev.tm.service.property;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.tsc.tambovtsev.tm.api.service.property.IApplicationPropertyService;
import ru.tsc.tambovtsev.tm.api.service.property.IDatabasePropertyService;

@Getter
@Service
@PropertySource("classpath:application.properties")
public class DatabasePropertyService implements IDatabasePropertyService {

    @Value("${environment['database.username']:root}")
    private String databaseUsername;

    @Value("${environment['database.password']:sadmin}")
    private String databasePassword;

    @Value("#{environment['database.url']}")
    private String databaseUrl;

    @Value("${environment['database.driver']:com.mysql.cj.jdbc.Driver}")
    private String databaseDriver;

    @Value("${environment['database.sql_dialect']:org.hibernate.dialect.MySQL5InnoDBDialect}")
    private String databaseSQLDialect;

    @Value("${environment['database.hbm2ddl_auto']:update}")
    private String hbm2ddlAuto;

    @Value("${environment['database.show_sql']:true}")
    private String showSql;

    @Value("${environment['database.format_sql']:true}")
    private String formatSql;

    @Value("${environment['database.second_lvl_cash']:true}")
    private String secondLvlCash;

    @Value("${environment['database.factory_class']:com.hazelcast.hibernate.HazelcastLocalCacheRegionFactory}")
    private String factoryClass;

    @Value("${environment['database.use_query_cash']:true}")
    private String useQueryCash;

    @Value("${environment['database.use_min_puts']:true}")
    private String useMinPuts;

    @Value("${environment['database.region_prefix':task-manager]}")
    private String regionPrefix;

    @Value("${environment['database.config_file_path']:hazelcast.xml}")
    private String configFilePath;

}
