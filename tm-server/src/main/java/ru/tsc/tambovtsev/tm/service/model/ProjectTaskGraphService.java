package ru.tsc.tambovtsev.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.tambovtsev.tm.api.repository.model.IProjectGraphRepository;
import ru.tsc.tambovtsev.tm.api.repository.model.ITaskGraphRepository;
import ru.tsc.tambovtsev.tm.api.service.model.IProjectTaskService;
import ru.tsc.tambovtsev.tm.exception.AbstractException;
import ru.tsc.tambovtsev.tm.exception.field.IdEmptyException;
import ru.tsc.tambovtsev.tm.model.Project;
import ru.tsc.tambovtsev.tm.model.Task;

import java.util.Optional;

@Service
public class ProjectTaskGraphService implements IProjectTaskService {

    @Nullable
    @Autowired
    private IProjectGraphRepository projectRepository;

    @Nullable
    @Autowired
    private ITaskGraphRepository repository;

    @Override
    @SneakyThrows
    @Transactional
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws AbstractException {
        Optional.ofNullable(taskId).orElseThrow(IdEmptyException::new);
        @Nullable final Task task = repository.findByUserIdAndId(userId, taskId);
        @Nullable final Project project = projectRepository.findByUserIdAndId(userId, projectId);
        if (task == null) return;
        task.setProject(project);
        repository.save(task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String taskId
    ) throws AbstractException {
        Optional.ofNullable(taskId).orElseThrow(IdEmptyException::new);
        @Nullable final Task task = repository.findByUserIdAndId(userId, taskId);
        if (task == null) return;
        task.setProject(null);
        repository.save(task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeProjectById(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws AbstractException {
        Optional.ofNullable(projectId).orElseThrow(IdEmptyException::new);
        @Nullable final Project project = projectRepository.findByUserIdAndId(userId, projectId);
        if (project == null) return;
        projectRepository.delete(project);
    }

}
