package ru.tsc.tambovtsev.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.api.comparator.NameComparator;
import ru.tsc.tambovtsev.tm.api.comparator.StatusComparator;

import java.util.Comparator;

@Getter
public enum SortTable {

    NAME("Sort by name", NameComparator.INSTANCE),
    STATUS("Sort by status", StatusComparator.INSTANCE);

    @Nullable
    private final String displayName;

    @Nullable
    private final Comparator comparator;

    SortTable(@Nullable String displayName, @Nullable Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    @Nullable
    public static SortTable toSort(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        for (final SortTable sortTable : values()) {
            if (sortTable.name().equals(value)) return sortTable;
        }
        return null;
    }

}
