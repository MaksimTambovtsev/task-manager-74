package ru.tsc.tambovtsev.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.tsc.tambovtsev.tm.api.service.ISenderService;
import ru.tsc.tambovtsev.tm.dto.logger.EntityLogDTO;
import ru.tsc.tambovtsev.tm.service.SenderService;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public final class MessageExecutor {

    private static final int THREAD_COUNT = 3;

    @NotNull
    private final ISenderService service = new SenderService();

    @NotNull
    private final ExecutorService es = Executors.newFixedThreadPool(THREAD_COUNT);

    public void sendMessage(@NotNull final Object object, @NotNull final String type) {
        es.submit(() -> {
            @NotNull final EntityLogDTO entity = service.createMessage(object, type);
            service.send(entity);
        });
    }

    public void stop() {
        es.shutdown();
    }

}
