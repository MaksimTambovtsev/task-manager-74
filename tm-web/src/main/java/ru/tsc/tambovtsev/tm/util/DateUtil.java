package ru.tsc.tambovtsev.tm.util;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.text.SimpleDateFormat;
import java.util.Date;

public interface DateUtil {

    @NotNull
    String PATTERN = "yyyy-MM-dd";

    @NotNull
    SimpleDateFormat FORMATTER = new SimpleDateFormat(PATTERN);

    @Nullable
    @SneakyThrows
    static Date toDate(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        return FORMATTER.parse(value);
    }

    @NotNull
    static String toString(@Nullable final Date value) {
        if (value == null) return "";
        return FORMATTER.format(value);
    }

}
