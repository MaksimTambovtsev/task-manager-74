package ru.tsc.tambovtsev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.tambovtsev.tm.model.Project;

import java.util.Collection;
import java.util.List;

public interface IProjectService {

    @NotNull
    @Transactional
    Project create(@Nullable String userId);

    @NotNull
    @Transactional
    Project create(@Nullable String userId, @NotNull String name);

    @NotNull
    @Transactional
    Project save(@NotNull Project project);

    @Nullable
    Collection<Project> findAll(@Nullable String userId);

    @Nullable
    Project findById(@Nullable String userId, @NotNull String id);

    @Transactional
    void removeById(@Nullable String userId, @NotNull String id);

    @Transactional
    void remove(@NotNull Project project);

    @Transactional
    void remove(@NotNull List<Project> projects);

    @Transactional
    void removeByUserId(@Nullable final String userId, @NotNull final String id);

    boolean existsByUserIdAndId(@Nullable String userId, @NotNull String id);

    @Transactional
    void clear(@Nullable String userId);

    long count(@Nullable String userId);

}
