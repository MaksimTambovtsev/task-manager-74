package ru.tsc.tambovtsev.tm.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import ru.tsc.tambovtsev.tm.enumerated.Status;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_project")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@EntityListeners({AuditingEntityListener.class})
public class Project extends AbstractWbsModel{

    private static final long serialVersionUID = 2;

    public Project(@NotNull final String name) {
        super(name);
    }

    public Project(@NotNull final String name, @Nullable final String description) {
        super(name, description);
    }

    public Project(@NotNull final String name, @NotNull final Status status, @Nullable final Date dateBegin) {
        super(name, status, dateBegin);
    }

    public Project(
            @NotNull final String name,
            @Nullable final String description,
            @Nullable final Date dateBegin,
            @Nullable final Date dateEnd
    ) {
        super(name, description, dateBegin, dateEnd);
    }

}
