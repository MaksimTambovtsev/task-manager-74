package ru.tsc.tambovtsev.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.tsc.tambovtsev.tm.api.service.IProjectService;
import ru.tsc.tambovtsev.tm.api.service.ITaskService;
import ru.tsc.tambovtsev.tm.enumerated.Status;
import ru.tsc.tambovtsev.tm.model.CustomUser;
import ru.tsc.tambovtsev.tm.model.Project;
import ru.tsc.tambovtsev.tm.model.Task;
import ru.tsc.tambovtsev.tm.service.ProjectService;

import java.util.Collection;

@Controller
public class TaskController {

    @Autowired
    private ITaskService taskService;

    @Autowired
    private IProjectService projectService;

    @Nullable
    private Collection<Task> getTasks(@NotNull final CustomUser customUser) {
        return taskService.findAll(customUser.getUserId());
    }

    @Nullable
    private Collection<Project> getProjects(@NotNull final CustomUser customUser) {
        return projectService.findAll(customUser.getUserId());
    }

    @NotNull
    private Status[] getStatuses() {
        return Status.values();
    }

    @GetMapping("/task/create")
    public String create(@AuthenticationPrincipal final CustomUser customUser) {
        taskService.create(customUser.getUserId());
        return "redirect:/tasks";
    }

    @GetMapping("/task/delete/{id}")
    public String delete(
            @AuthenticationPrincipal final CustomUser customUser,
            @NotNull @PathVariable("id") final String id
    ) {
        taskService.removeById(customUser.getUserId(), id);
        return "redirect:/tasks";
    }

    @PostMapping("task/edit/{id}")
    public String edit(
            @AuthenticationPrincipal final CustomUser customUser,
            @NotNull @ModelAttribute("task") final Task task,
            @NotNull final BindingResult result
    ) {
        if (task.getProjectId().isEmpty()) task.setProjectId(null);
        task.setUserId(customUser.getUserId());
        taskService.save(task);
        return "redirect:/tasks";
    }

    @NotNull
    @GetMapping("task/edit/{id}")
    public ModelAndView edit(
            @AuthenticationPrincipal final CustomUser customUser,
            @NotNull @PathVariable("id") final String id
    ) {
        @NotNull final Task task = taskService.findById(customUser.getUserId(), id);
        @NotNull final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-edit");
        modelAndView.addObject("task", task);
        modelAndView.addObject("projects", getProjects(customUser));
        modelAndView.addObject("statuses", getStatuses());
        return modelAndView;
    }

    @NotNull
    @GetMapping("/tasks")
    public ModelAndView list(@AuthenticationPrincipal final CustomUser customUser) {
        return new ModelAndView("task-list", "tasks", getTasks(customUser));
    }

}
