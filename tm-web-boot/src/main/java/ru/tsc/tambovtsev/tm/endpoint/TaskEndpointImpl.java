package ru.tsc.tambovtsev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tsc.tambovtsev.tm.api.endpoint.ITaskEndpointImpl;
import ru.tsc.tambovtsev.tm.model.Task;
import ru.tsc.tambovtsev.tm.service.TaskService;
import ru.tsc.tambovtsev.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/task")
@WebService(endpointInterface = "ru.tsc.tambovtsev.tm.api.endpoint.ITaskEndpointImpl")
public class TaskEndpointImpl implements ITaskEndpointImpl {

    @NotNull
    @Autowired
    private TaskService taskService;

    @Override
    @WebMethod
    @GetMapping("/findAll")
    public List<Task> findAll() {
        return taskService
                .findAll(UserUtil.getUserId())
                .stream()
                .collect(Collectors.toList());
    }

    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    public Task findById(@NotNull @PathVariable("id") final String id) {
        return taskService.findById(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @GetMapping("/existsById/{id}")
    public boolean existsById(@NotNull @PathVariable("id") final String id) {
        return taskService.existsByUserIdAndId(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @PostMapping("/save")
    public Task save(@NotNull @RequestBody final Task task) {
        task.setUserId(UserUtil.getUserId());
        return taskService.save(task);
    }

    @Override
    @WebMethod
    @DeleteMapping("/delete")
    public void delete(@NotNull @RequestBody final Task task) {
        taskService.remove(task);
    }

    @Override
    @WebMethod
    @DeleteMapping("/deleteAll")
    public void deleteAll(@NotNull @RequestBody final List<Task> tasks) {
        taskService.remove(tasks);
    }

    @Override
    @WebMethod
    @DeleteMapping("/clear")
    public void clear() {
        taskService.clear(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @DeleteMapping("/deleteById/{id}")
    public void deleteById(@NotNull final String id) {
        taskService.removeById(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @GetMapping("/count")
    public long count() {
        return taskService.count(UserUtil.getUserId());
    }
    
}
