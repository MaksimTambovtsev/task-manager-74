package ru.tsc.tambovtsev.tm.util;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import ru.tsc.tambovtsev.tm.exception.AccessException;
import ru.tsc.tambovtsev.tm.model.CustomUser;


public class UserUtil {

    public static String getUserId() {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        final Object principal = authentication.getPrincipal();
        if (principal == null) throw new AccessException();
        if (!(principal instanceof CustomUser)) throw new AccessException();
        final CustomUser customUser = (CustomUser) principal;
        return customUser.getUserId();
    }

}
