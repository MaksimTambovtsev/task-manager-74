package ru.tsc.tambovtsev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tsc.tambovtsev.tm.api.endpoint.IProjectEndpointImpl;
import ru.tsc.tambovtsev.tm.model.Project;
import ru.tsc.tambovtsev.tm.service.ProjectService;
import ru.tsc.tambovtsev.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/project")
@WebService(endpointInterface = "ru.tsc.tambovtsev.tm.api.endpoint.IProjectEndpointImpl")
public class ProjectEndpointImpl implements IProjectEndpointImpl {

    @NotNull
    @Autowired
    private ProjectService projectService;

    @Override
    @WebMethod
    @GetMapping("/findAll")
    public List<Project> findAll() {
        return projectService
                .findAll(UserUtil.getUserId())
                .stream()
                .collect(Collectors.toList());
    }

    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    public Project findById(@NotNull @PathVariable("id") final String id) {
        return projectService.findById(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @GetMapping("/existsById/{id}")
    public boolean existsById(@NotNull @PathVariable("id") final String id) {
        return projectService.existsByUserIdAndId(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @PostMapping("/save")
    public Project save(@NotNull @RequestBody final Project project) {
        project.setUserId(UserUtil.getUserId());
        return projectService.save(project);
    }

    @Override
    @WebMethod
    @DeleteMapping("/delete")
    public void delete(@NotNull @RequestBody final Project project) {
        projectService.remove(project);
    }

    @Override
    @WebMethod
    @DeleteMapping("/deleteAll")
    public void deleteAll(@NotNull @RequestBody final List<Project> projects) {
        projectService.remove(projects);
    }

    @Override
    @WebMethod
    @DeleteMapping("/clear")
    public void clear() {
        projectService.clear(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @DeleteMapping("/deleteById/{id}")
    public void deleteById(@NotNull final String id) {
        projectService.removeById(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @GetMapping("/count")
    public long count() {
        return projectService.count(UserUtil.getUserId());
    }

}
