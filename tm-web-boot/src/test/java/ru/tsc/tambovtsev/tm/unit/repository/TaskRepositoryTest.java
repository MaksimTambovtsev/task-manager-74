package ru.tsc.tambovtsev.tm.unit.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.tambovtsev.tm.api.repository.ITaskRepository;
import ru.tsc.tambovtsev.tm.marker.UnitCategory;
import ru.tsc.tambovtsev.tm.model.Task;
import ru.tsc.tambovtsev.tm.util.UserUtil;

import java.util.List;

@Transactional
@SpringBootTest
@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class TaskRepositoryTest {

    @NotNull
    @Autowired
    private ITaskRepository repository;

    @NotNull
    @Autowired
    private AuthenticationManager manager;

    @NotNull
    private final Task task = new Task("Task1");

    @Before
    public void init() {
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("test", "test");
        @NotNull final Authentication authentication = manager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        task.setUserId(UserUtil.getUserId());
        repository.save(task);
    }

    @After
    public void clean() {
        repository.deleteAll();
    }

    @Test
    public void findAllTest() {
        @Nullable final List<Task> taskList = repository.findAll();
        Assert.assertEquals(1, taskList.size());
    }

    @Test
    public void findAllByUserIdTest() {
        @Nullable final List<Task> taskList = repository.findAllByUserId(UserUtil.getUserId());
        Assert.assertEquals(1, taskList.size());
    }

    @Test
    public void findByUserIdAndIdTest() {
        @Nullable final Task taskFind = repository.findByUserIdAndId(UserUtil.getUserId(), task.getId());
        Assert.assertNotNull(taskFind);
        Assert.assertEquals(taskFind.getId(), task.getId());
    }

    @Test
    public void deleteByUserIdTest() {
        repository.deleteByUserIdAndId(task.getUserId(), task.getId());
        Assert.assertEquals(0, repository.countByUserId(UserUtil.getUserId()));
    }

    @Test
    public void deleteByIdTest() {
        repository.deleteById(task.getId());
        Assert.assertEquals(0, repository.countByUserId(UserUtil.getUserId()));
    }

    @Test
    public void deleteAllByUserIdTest() {
        repository.deleteAllByUserId(UserUtil.getUserId());
        Assert.assertEquals(0, repository.countByUserId(UserUtil.getUserId()));
    }

}
