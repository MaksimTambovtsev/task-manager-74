package ru.tsc.tambovtsev.tm.unit.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.tsc.tambovtsev.tm.marker.UnitCategory;
import ru.tsc.tambovtsev.tm.model.Task;
import ru.tsc.tambovtsev.tm.util.UserUtil;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class TaskEndpointTest {

    @NotNull
    @Autowired
    private AuthenticationManager manager;

    @NotNull
    private MockMvc mockMvc;

    @NotNull
    @Autowired
    private WebApplicationContext context;

    @NotNull
    private static final String PROJECT_URL = "http://localhost:8080/api/task/";

    @NotNull
    private final Task task = new Task("Task1", "Description");

    @Before
    public void init() {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("test", "test");
        @NotNull final Authentication authentication = manager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        task.setUserId(UserUtil.getUserId());
        save(task);
    }

    @After
    public void clean() {
        clear();
    }

    @SneakyThrows
    private void clear() {
        @NotNull final String url = PROJECT_URL + "clear";
        mockMvc.perform(
                        MockMvcRequestBuilders
                                .delete(url)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isOk());
    }

    @SneakyThrows
    private void save(@NotNull final Task task) {
        @NotNull final String url = PROJECT_URL + "save";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writeValueAsString(task);
        mockMvc.perform(
                        MockMvcRequestBuilders
                                .post(url)
                                .content(json)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Nullable
    @SneakyThrows
    private Task findById(@NotNull final String id) {
        @NotNull String url = PROJECT_URL + "findById/" + id;
        @NotNull final String json =
                mockMvc.perform(
                                MockMvcRequestBuilders.get(url)
                                        .contentType(MediaType.APPLICATION_JSON)
                        )
                        .andDo(print())
                        .andExpect(status().isOk())
                        .andReturn()
                        .getResponse()
                        .getContentAsString();
        if (json.isEmpty()) return null;
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, Task.class);
    }

    @NotNull
    @SneakyThrows
    private List<Task> findAll() {
        @NotNull final String url = PROJECT_URL + "findAll";
        @NotNull final String json = mockMvc.perform(
                        MockMvcRequestBuilders.get(url)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return Arrays.asList(objectMapper.readValue(json, Task[].class));
    }

    @SneakyThrows
    private long count() {
        @NotNull final String url = PROJECT_URL + "count";
        @NotNull final String json = mockMvc.perform(
                        MockMvcRequestBuilders.get(url)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();
        if (json.isEmpty()) return 0;
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, Long.class);
    }

    @Test
    public void findAllTest() {
        @NotNull final List<Task> taskList = findAll();
        Assert.assertEquals(1, taskList.size());
    }

    @Test
    public void findById() {
        @Nullable final Task task1Find= findById(task.getId());
        Assert.assertNotNull(task1Find);
        Assert.assertEquals(task1Find.getId(), task.getId());
    }

    @Test
    public void add() {
        @NotNull final Task taskNew = new Task("Task2", "Description2");
        taskNew.setUserId(UserUtil.getUserId());
        save(taskNew);
        @Nullable final Task task = findById(taskNew.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(taskNew.getId(), task.getId());
    }

    @Test
    @SneakyThrows
    public void delete() {
        Assert.assertEquals(1, count());
        @NotNull final String url = PROJECT_URL + "delete";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writeValueAsString(task);
        mockMvc.perform(
                        MockMvcRequestBuilders
                                .delete(url)
                                .content(json)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isOk());
        Assert.assertEquals(0, count());
    }

    @Test
    @SneakyThrows
    public void deleteAll() {
        @NotNull final Task taskNew = new Task("Task3", "Description3");
        taskNew.setUserId(UserUtil.getUserId());
        save(taskNew);
        Assert.assertEquals(2, count());
        @NotNull final List<Task> tasks = Collections.singletonList(taskNew);
        @NotNull final String url = PROJECT_URL + "deleteAll";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writeValueAsString(tasks);
        mockMvc.perform(
                        MockMvcRequestBuilders
                                .delete(url)
                                .content(json)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isOk());
        Assert.assertEquals(1, count());
    }

    @Test
    public void clearTest() {
        clear();
        Assert.assertEquals(0, count());
    }

    @Test
    @SneakyThrows
    public void deleteById() {
        @NotNull final String url = PROJECT_URL + "deleteById/" + task.getId();
        mockMvc.perform(
                        MockMvcRequestBuilders
                                .delete(url)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isOk());
        Assert.assertEquals(0, count());
    }

    @Test
    public void countTest() {
        Assert.assertEquals(1, count());
    }
    
}
