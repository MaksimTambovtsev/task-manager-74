package ru.tsc.tambovtsev.tm.unit.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.tsc.tambovtsev.tm.api.service.IUserService;
import ru.tsc.tambovtsev.tm.marker.UnitCategory;
import ru.tsc.tambovtsev.tm.model.User;
import ru.tsc.tambovtsev.tm.util.UserUtil;

import java.util.Collection;

@SpringBootTest
@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class UserServiceTest {

    @NotNull
    @Autowired
    private IUserService service;

    @NotNull
    @Autowired
    private AuthenticationManager manager;

    @NotNull
    private final User user = new User("User1", "cewf3423w23e12edwq", "user1@useg.com");

    @Before
    public void init() {
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("admin", "admin");
        @NotNull final Authentication authentication = manager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        service.save(UserUtil.getUserId(), user);
    }

    @After
    public void clean() {
        service.remove(UserUtil.getUserId(), user);
    }

    @Test
    public void findAllTest() {
        @NotNull final String userId = UserUtil.getUserId();
        @Nullable final Collection<User> userList = service.findAll(userId);
        Assert.assertNotNull(userList);
        Assert.assertEquals(3, userList.size());
    }

    @Test
    public void findByLoginTest() {
        @NotNull final String userId = UserUtil.getUserId();
        @Nullable final User userFind = service.findByLogin(userId, user.getLogin());
        Assert.assertNotNull(userFind);
        Assert.assertEquals(userFind.getId(), user.getId());
    }

    @Test
    public void removeByLoginTest() {
        @NotNull final String userId = UserUtil.getUserId();
        service.removeByLogin(userId, user.getLogin());
        Assert.assertEquals(2, service.count(userId));
    }

    @Test
    public void countTest() {
        Assert.assertEquals(3, service.count(UserUtil.getUserId()));
    }

}
